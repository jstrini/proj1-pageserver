# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

## Getting Started ##
* Clone this repository on to your machine
* Copy and rename credentials-skel.ini and rename it to credentials.ini
* Fill out credentials.ini with your info
* Execute `make run` from the project's root directory

## Author: Joe Strini, jstrini@uoregon.edu ##
